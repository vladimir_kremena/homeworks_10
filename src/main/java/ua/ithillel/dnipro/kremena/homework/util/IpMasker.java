package ua.ithillel.dnipro.kremena.homework.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IpMasker {
    private static final Pattern ipPattern = Pattern.compile(
            "(^|\\s|\\W)" +
                    "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\." +
                    "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\." +
                    "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\." +
                    "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\b");

    public static String disguiseIp(String text) {
        return text.replaceAll(ipPattern.toString(), "$1$2.*.*.$5");
    }
}