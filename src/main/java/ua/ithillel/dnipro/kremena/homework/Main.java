package ua.ithillel.dnipro.kremena.homework;

import ua.ithillel.dnipro.kremena.homework.util.FileUtil;
import ua.ithillel.dnipro.kremena.homework.util.IpMasker;

public class Main {

    public static void main(String[] args) {

        String originText = FileUtil.readText("./repository/origin_text.txt");

        String disguisedIpText = IpMasker.disguiseIp(originText);

        FileUtil.saveText(disguisedIpText, "./repository/disguised_ip_text.txt");

    }
}