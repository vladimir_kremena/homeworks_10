package ua.ithillel.dnipro.kremena.homework.util;

import java.io.*;

public class FileUtil {

    public static String readText(String path) {
        StringBuilder text = new StringBuilder();
        try(BufferedReader reader = new BufferedReader(new FileReader(new File(path)))) {
            while(true) {
                String line = reader.readLine();
                if (line == null) break;
                text.append(line.concat("\n"));
            }
        } catch (IOException iOE) {
            iOE.printStackTrace();
        }
        return text.toString();
    }

    public static void saveText(String text, String path) {
        File file = new File(path);
        createDirectoryAndFile(file);
        try(BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
            writer.write(text);
            writer.flush();
        } catch (IOException iOE) {
            iOE.printStackTrace();
        }
    }

    private static void createDirectoryAndFile(File file) {
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException iOE) {
                System.out.println("File creation error: " + iOE.getMessage());
            }
        }
    }
}